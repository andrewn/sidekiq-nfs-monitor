Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"

  config.vm.provider "virtualbox" do |v|
    v.memory = 4096
    v.cpus = 4
  end

  config.vm.provision "shell", inline: <<-SHELL
    echo "deb [trusted=yes] https://repo.iovisor.org/apt/xenial xenial-nightly main" > /etc/apt/sources.list.d/iovisor.list
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4052245BD4284CDD
    apt-get update -y
    apt-get install -y \
      gnupg lsb-release ca-certificates \
      bcc-tools libbcc-examples linux-headers-$(uname -r) build-essential \
      nfs-kernel-server nfs-common

    curl -sO https://storage.googleapis.com/golang/go1.14.6.linux-amd64.tar.gz
    tar -xf go1.14.6.linux-amd64.tar.gz
    mv go /usr/local
    rm -f go1.14.6.linux-amd64.tar.gz

    echo "export PATH=$PATH:/usr/local/go/bin" >> /home/vagrant/.profile

    mkdir -p /export/test
    chmod -R 777 /export/test

    echo "/export/test 127.0.0.1/32(rw,sync,no_subtree_check,no_root_squash)" >> /etc/exports
    service nfs-kernel-server restart
    exportfs -rv

    sudo mount -t nfs -vvvv localhost:/export/test /mnt

    mkdir -p /var/log/gitlab/sidekiq-cluster
    touch /var/log/gitlab/sidekiq-cluster/current
    chmod a+w /var/log/gitlab/sidekiq-cluster/current
  SHELL
end
