package main

import (
	"fmt"

	bpf "github.com/iovisor/gobpf/bcc"
)

//go:generate esc -o static.go -pkg main nfsmonitor.c.bpf

type nfsEvent struct {
	Pid uint32
}

func attachKprobe(m *bpf.Module, fnName string, kprobeName string) error {
	fd, err := m.LoadKprobe(fnName)
	if err != nil {
		return err
	}

	// passing -1 for maxActive signifies to use the default
	// according to the kernel kprobes documentation
	// NOTE: when using -1, the probe is not being removed:
	// https://github.com/iovisor/gobpf/issues/174
	err = m.AttachKprobe(kprobeName, fd, 32)
	if err != nil {
		return fmt.Errorf("unable to attach kprobe %s to function %s: %w", kprobeName, fnName, err)
	}
	return nil
}

func attachKretprobe(m *bpf.Module, fnName string, kprobeName string) error {
	fd, err := m.LoadKprobe(fnName)
	if err != nil {
		return err
	}

	// passing -1 for maxActive signifies to use the default
	// according to the kernel kprobes documentation
	// NOTE: when using -1, the probe is not being removed:
	// https://github.com/iovisor/gobpf/issues/174
	err = m.AttachKretprobe(kprobeName, fd, 32)
	if err != nil {
		return fmt.Errorf("unable to attach kretprobe %s to function %s: %w", kprobeName, fnName, err)
	}

	return err
}

func loadBPFModule() (*bpf.Module, error) {
	m := bpf.NewModule(FSMustString(false, "/nfsmonitor.c.bpf"), []string{})
	if m == nil {
		return nil, fmt.Errorf("Failed to load eBPF module")
	}

	/* Attach kprobes */
	if err := attachKprobe(m, "trace_rw_entry", "nfs_file_read"); err != nil {
		return nil, err
	}

	if err := attachKprobe(m, "trace_rw_entry", "nfs_file_write"); err != nil {
		return nil, err
	}

	if err := attachKprobe(m, "trace_file_open_entry", "nfs4_file_open"); err != nil {
		return nil, err
	}

	if err := attachKprobe(m, "trace_file_open_entry", "nfs_file_open"); err != nil {
		return nil, err
	}

	if err := attachKprobe(m, "trace_getattr_entry", "nfs_getattr"); err != nil {
		return nil, err
	}

	return m, nil
}

func createPerfMapChannels(m *bpf.Module) (*bpf.PerfMap, chan []byte, error) {
	processStartEventsTable := bpf.NewTable(m.TableId("events"), m)
	byteChan := make(chan []byte)
	perfMap, err := bpf.InitPerfMap(processStartEventsTable, byteChan)

	if err != nil {
		return nil, nil, fmt.Errorf("unable to connect to perf map: %w", err)
	}

	return perfMap, byteChan, nil
}
