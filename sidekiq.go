package main

import (
	"encoding/json"
	"sync"
)

type SidekiqLog struct {
	JobStatus string `json:"job_status"`
	Pid       uint32 `json:"pid"`
	Queue     string `json:"queue"`
}

func parseLogLine(line string) (SidekiqLog, error) {
	var logLine SidekiqLog
	err := json.Unmarshal([]byte(line), &logLine)
	return logLine, err
}

var m sync.Mutex = sync.Mutex{}
var jobMap map[uint32]string = map[uint32]string{}

// Note: at present this will leak memory as pids are never cleaned up
func associateQueueWithPid(queue string, pid uint32) {
	m.Lock()
	jobMap[pid] = queue
	m.Unlock()
}

func lookupQueueForPid(pid uint32) string {
	m.Lock()
	result, ok := jobMap[pid]
	if ok {
		delete(jobMap, pid)
	}
	m.Unlock()
	return result
}
