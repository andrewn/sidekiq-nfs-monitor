#!/bin/bash

pid=$$

command echo "{\"class\":\"TestClass\",\"queue\":\"test_queue\",\"pid\":${pid},\"job_status\":\"start\"}" >> /var/log/gitlab/sidekiq-cluster/current
command echo "${pid}" >> /mnt/test.${pid}
command echo "{\"class\":\"TestWorker\",\"queue\":\"test_queue\",\"pid\":${pid},\"job_status\":\"done\"}" >> /var/log/gitlab/sidekiq-cluster/current
