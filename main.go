package main

import (
	"bytes"
	"encoding/binary"
	"flag"

	"github.com/hpcloud/tail"
	"github.com/sirupsen/logrus"
	logkit "gitlab.com/gitlab-org/labkit/log"
	"gitlab.com/gitlab-org/labkit/monitoring"
)

func main() {
	metricsPort := flag.String("listen-addr", ":10282", "Address to listen on")
	flag.Parse()

	// Initialize the global logger
	closer, err := logkit.Initialize(
		logkit.WithFormatter("json"),
		logkit.WithLogLevel("info"),
	)
	defer closer.Close()

	// Start listener
	logrus.Infof("Starting metrics on %s", *metricsPort)
	go func() {
		logrus.WithError(monitoring.Start(
			monitoring.WithListenerAddress(*metricsPort),
		)).Fatal("Unable to start monitor")
	}()

	logrus.Infof("Loading BPF Module...")
	m, err := loadBPFModule()
	if err != nil {
		logrus.WithError(err).Fatalf("Unable to load BPF Module")
		return
	}
	defer m.Close()

	sidekiqLogFile := "/var/log/gitlab/sidekiq-cluster/current"
	logrus.Infof("Attempting to tail file %s", sidekiqLogFile)
	t, err := tail.TailFile(sidekiqLogFile, tail.Config{Follow: true, ReOpen: true})
	if err != nil {
		logrus.WithError(err).Fatalf("Unable to tail file")
		return
	}
	logrus.Infof("Watching %s", sidekiqLogFile)

	logrus.Infof("Creating something called a Perf map...")
	processStartEventsMap, eventsChannel, err := createPerfMapChannels(m)
	if err != nil {
		logrus.WithError(err).Fatalf("Perf map creation failed")
		return
	}

	logrus.Infof("Watching for NFS events...")
	go func() {
		var event nfsEvent
		for {
			data := <-eventsChannel
			err := binary.Read(bytes.NewBuffer(data), binary.LittleEndian, &event)
			if err != nil {
				logrus.WithError(err).Warn("failed to decode received data")
				continue
			}

			var queue = lookupQueueForPid(event.Pid)
			if queue != "" {
				nfsAccessesDetected.WithLabelValues(queue).Inc()
			}
		}
	}()

	processStartEventsMap.Start()
	defer processStartEventsMap.Stop()

	for line := range t.Lines {
		logLine, err := parseLogLine(line.Text)
		if err != nil {
			logrus.WithError(err).Warn("Unable to parse line")
			continue
		}

		// Ignore invalid jobs
		if logLine.Pid == 0 || logLine.Queue == "" {
			continue
		}

		// Only track on job start
		if logLine.JobStatus == "start" {
			jobsStarted.WithLabelValues(logLine.Queue).Inc()
			associateQueueWithPid(logLine.Queue, logLine.Pid)
		}

		// We should also dissociate on done
	}
}
