module gitlab.com/andrewn/sidekiq-nfs-monitor

go 1.13

require (
	github.com/hpcloud/tail v1.0.0
	github.com/iovisor/gobpf v0.0.0-20191017091429-c3024dcc6881
	github.com/prometheus/client_golang v1.0.0
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/gitlab-org/labkit v0.0.0-20200728123508-886c482d483c
	golang.org/x/sys v0.0.0-20200728102440-3e129f6d46b1 // indirect
	golang.org/x/tools v0.0.0-20200729194436-6467de6f59a7 // indirect
)
