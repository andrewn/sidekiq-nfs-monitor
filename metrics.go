package main

import "github.com/prometheus/client_golang/prometheus"

var jobsStarted = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Subsystem: "sidekiq_nfs_monitor",
		Name:      "jobs_started",
		Help:      "Job started",
	},
	[]string{"queue"},
)

var nfsAccessesDetected = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Subsystem: "sidekiq_nfs_monitor",
		Name:      "nfs_access_detected",
		Help:      "NFS access detected",
	},
	[]string{"queue"},
)

func init() {
	prometheus.MustRegister(jobsStarted)
	prometheus.MustRegister(nfsAccessesDetected)
}
